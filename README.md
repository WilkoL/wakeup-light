# Wakeup Light

Wakeup light with red, green, blue and white leds that slowly increase in intensity 
at the requested wakeup time until they are on 100%. After 1 hour they all switch off.