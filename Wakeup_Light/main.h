/*
 * main.h
 *
 */


#ifndef MAIN_H_
#define MAIN_H_

#define F_CPU 8000000UL									//clock op 8MHz, interne clock dus niet nauwkeurig
#define DS1307 0x68
#define WAKEHOUR 4										//locatie in eeprom van de wektijd (uren)
#define WAKEMINUTE 8									//locatie in eeprom van de wektijd (minuten)
#define ACTIVE 1
#define NOTACTIVE 0

#include <inttypes.h>
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include "lcd.h"
#include "USI_I2C.h"



#endif /* MAIN_H_ */