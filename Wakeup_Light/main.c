/*
 *
 * Wakeup clock
 * ATTINY4313
 * DS1307 als RTC
 * LCD Display 16x2
 *
 * twee knopjes voor menu-keuze en waarde instelling
 * drie pwd gestuurde led-poorten (wit, rood en blauw/groen)
 * op de ingesteld tijd gaan de leds langzaam aan (behalve in het weekeinde)
 *
 * aanpassing 1
 * de "PWM_Interrupt_Routine" past nu elke tweede keer dat deze actief
 * is de helderheid van de leds aan, het duurt nu dus langer voor alle
 * leds op volle sterkte zijn
 *
 * aanpassing 2
 * (ook een blauwe led vervangen die begon te blink_led als deze warm werd.)
 *
 * change 3
 * did some cleaning up of the code
 * added header files for USI and main
 * fixed eeprom warning about incompatible pointers
 *
 *  Author: wilko
 */


#include "main.h"

void init(void);
void clockmenu(void);
uint8_t BCD2DEC(uint8_t bcd);
uint8_t DEC2BCD(uint8_t dec);
void get_rtc_time(void);
void set_rtc_time(void);
void write_eeprom( uint16_t, uint8_t);
uint8_t read_eeprom(uint16_t);
void blink_led(void);
void display_lcd();
void display_number(uint8_t);
void display_weekday(uint8_t, uint8_t, uint8_t);

struct tijd_t
{
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t weekday;
};

volatile uint8_t menu;
volatile uint8_t menu_button;
volatile uint8_t value_button;
volatile uint8_t leds_on;

struct tijd_t current_time;
struct tijd_t wakeup_time;




int main()
{
	wdt_reset();
	leds_on = NOTACTIVE;
	init();
	I2C_init();

	while (1)
	{

		if (menu_button == 0)							//display current time and wakeup time
		{
			wdt_reset();
			get_rtc_time();
			display_lcd();
			_delay_ms(33);

			if ((current_time.hour == read_eeprom(WAKEHOUR)) && (current_time.minute == read_eeprom(WAKEMINUTE)) && (current_time.weekday != 6) && (current_time.weekday != 7))
			{
				leds_on = ACTIVE;						//switch on the wakeup-leds
				PORTB |= (1 << PB6);					//LCD-backlight on
			}
			if ((current_time.hour > read_eeprom(WAKEHOUR)) && (current_time.minute > read_eeprom(WAKEMINUTE)))
			{
				leds_on = NOTACTIVE;					//after 1 hour switch wakeup leds off
				PORTB &= ~(1 << PB6);					//LCD-backlight off
			}
		}
		else											//display menu (set time and wakeup time)
		{
			PORTB |= (1 << PB6);						//LCD-backlight on
			clockmenu();
			PORTB &= ~(1 << PB6);						//LCD-backlight off
		}
	}
}



void init ()
{
	wdt_enable(WDTO_8S);
	wdt_reset();

	TCCR0A |= (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);		//clear OC0B on compare match, fast pwm mode
	TCCR0B |= (1 << CS01) | (1 << CS00);						//prescaler op 64 dus counter op 125 kHz, tellen tot 256 geeft 488 Hz
	TCNT0 = 0;
	OCR0A = 0;
	OCR0B = 0;
	TIMSK |= (1 << TOIE0);										//interrupt on overflow TIMER0 (488 keer per seconde, voor debouncing)

	TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11);		//clear OC1A en OC1B on compare match
	TCCR1B |= (1 << CS10) | (1 << WGM13) | ( 1 << WGM12);		//fast pwm mode, no prescaling dus de counter loopt op 8MHz
	TCCR1C = 0x00;												//teller tot 65536 geeft 122 Hz
	OCR1A = 0;
	OCR1B = 0;
	TCNT1 = 0;
	ICR1  = 0xFFFF;										//TOP = 65535  BOTTOM = 0
	TIMSK |= (1 << TOIE1);								//interrupt bij TIMER1 overflow 8000000/65536 = 122 Hz


	DDRB |= (1 << PB6);									//PB6 output for LCD backlight
	DDRD = 0;
	PORTD |= (1 << PD3) | (1 << PD6);					//pullup on PD3 (menu_button) PD6 (value_button)

	sei();

	lcd_init(LCD_DISP_ON);								// initialize LCD with the cursor off
	lcd_clrscr();
	menu = NOTACTIVE;
}


uint8_t BCD2DEC(uint8_t bcd)
{
	return (bcd & 0xF) + ((bcd >> 4) * 10);
}

uint8_t DEC2BCD(uint8_t dec)
{
	return (dec % 10) | ((dec / 10) << 4);
}

void get_rtc_time(void)
{
	uint8_t buf[5];

	buf[0] = (DS1307 << 1) | 0;							//rtc-address + write
	buf[1] = 0x00;										//seconds_register
	I2C_xfer(buf, 2);									//send 2 bytes

	buf[0] |= 1;										//rtc-address + read
	I2C_xfer(buf, 5);									//read 5 bytes
	current_time.second	 = BCD2DEC(buf[1]);
	current_time.minute	 = BCD2DEC(buf[2]);
	current_time.hour    = BCD2DEC(buf[3]);
	current_time.weekday = BCD2DEC(buf[4]);
}

void set_rtc_time(void)
{
	uint8_t buf[6];

	buf[0] = (DS1307 << 1) | 0;							//rtc-address + write
	buf[1] = 0x00;										//seconds_register
	buf[2] = 0x00;										//seconds to 0, CH=0 clock enable
	buf[3] = DEC2BCD(current_time.minute);
	buf[4] = DEC2BCD(current_time.hour);
	buf[5] = DEC2BCD(current_time.weekday);
	I2C_xfer(buf, 6);									//write 6 bytes
}

void display_lcd()
{
	uint16_t base = 10;
	char buffer[10];
	lcd_clrscr();
	lcd_puts("     ");
	if (current_time.hour < 10) lcd_puts(" ");
	lcd_puts(ltoa(current_time.hour, buffer, base));
	lcd_puts(":");
	if (current_time.minute < 10) lcd_puts("0");
	lcd_puts(ltoa(current_time.minute, buffer, base));
	lcd_puts(":");
	if (current_time.second < 10) lcd_puts("0");
	lcd_puts(ltoa(current_time.second, buffer, base));
	display_weekday(4, 1, current_time.weekday);

}

void write_eeprom( uint16_t location, uint8_t data)		//write data to location in eeprom
{
	eeprom_busy_wait();
	eeprom_update_byte((uint8_t *) location, data);
}

uint8_t read_eeprom(uint16_t location)					//read data from location in eeprom
{
	uint8_t data = 0;
	eeprom_busy_wait();
	data = eeprom_read_byte((uint8_t *) location);
	return data;
}

void blink_led()
{
	uint8_t i;

	for (i = 1; i < 19; i++)
	{
		PORTB ^= (1 << PB6);							//blink debug led
		_delay_ms(100);
	}
}


void display_number(uint8_t x)
{
	uint16_t base = 10;
	char buffer[10];

	if (x < 10) lcd_puts("0");
	lcd_puts(ltoa(x, buffer, base ));
}


void display_weekday(uint8_t column, uint8_t row, uint8_t day)
{
	lcd_gotoxy(column, row);
	switch(day)
	{
		case 1:
		lcd_puts("maandag  ");
		break;

		case 2:
		lcd_puts("dinsdag  ");
		break;

		case 3:
		lcd_puts("woensdag ");
		break;

		case 4:
		lcd_puts("donderdag");
		break;

		case 5:
		lcd_puts("vrijdag  ");
		break;

		case 6:
		lcd_puts("zaterdag ");
		break;

		case 7:
		lcd_puts("zondag   ");
		break;

		default:
		break;
	}
}

void clockmenu(void)
{
	menu = ACTIVE;
	value_button = 0;
	switch (menu_button)
	{
		case 1:
		wdt_reset();
		lcd_clrscr();
		lcd_gotoxy(0,0);
		lcd_puts("invoer tijd");
		lcd_gotoxy(0,1);
		lcd_puts("uren     ");
		get_rtc_time();
		value_button = current_time.hour;
		while (menu_button == 1)
		{
			if (value_button > 23) value_button = 0;
			lcd_gotoxy(9,1);
			display_number(value_button);
		}
		cli();
		current_time.hour = value_button;
		set_rtc_time();
		sei();

		value_button = 0;
		break;

		case 2:
		wdt_reset();
		lcd_clrscr();
		lcd_gotoxy(0,0);
		lcd_puts("invoer tijd");
		lcd_gotoxy(0,1);
		lcd_puts("minuten  ");
		get_rtc_time();
		value_button = current_time.minute;
		while (menu_button == 2)
		{
			if (value_button > 59) value_button = 0;
			lcd_gotoxy(9,1);
			display_number(value_button);
		}
		cli();
		current_time.minute = value_button;
		set_rtc_time();
		sei();

		value_button = 0;
		break;

		case 3:
		wdt_reset();
		lcd_clrscr();
		lcd_gotoxy(0,0);
		lcd_puts("invoer wektijd");
		lcd_gotoxy(0,1);
		lcd_puts("uren     ");
		value_button = read_eeprom(WAKEHOUR);
		while (menu_button == 3)
		{
			if (value_button > 23) value_button = 0;
			lcd_gotoxy(9,1);
			display_number(value_button);
		}

		write_eeprom(WAKEHOUR, value_button);
		value_button = 0;
		break;

		case 4:
		wdt_reset();
		lcd_clrscr();
		lcd_gotoxy(0,0);
		lcd_puts("invoer wektijd");
		lcd_gotoxy(0,1);
		lcd_puts("minuten  ");
		value_button = read_eeprom(WAKEMINUTE);
		while (menu_button == 4)
		{
			if (value_button > 59) value_button = 0;
			lcd_gotoxy(9,1);
			display_number(value_button);
		}
		write_eeprom(WAKEMINUTE, value_button);
		value_button = 0;
		break;

		case 5:
		wdt_reset();
		lcd_clrscr();
		lcd_gotoxy(0,0);
		lcd_puts("day van de week");
		get_rtc_time();
		value_button = current_time.weekday;
		while (menu_button == 5)
		{
			if (value_button > 7) value_button = 0;
			display_weekday(0, 1, value_button);
		}
		cli();
		current_time.weekday = value_button;
		set_rtc_time();
		sei();

		value_button = 0;
		break;

		default:
		menu = NOTACTIVE;
		menu_button = 0;
		break;
	}
}


ISR (TIMER0_OVF_vect)									//debouncing buttons (488 Hz = circa 2 ms, 8x samplen = 16ms)
{
	static uint8_t button1 = 0xFF;
	static uint8_t button2 = 0xFF;
	static uint8_t new1 = 0;
	static uint8_t new2 = 0;
	static uint8_t old1 = 0;
	static uint8_t old2 = 0;

	button1 = button1 << 1;								//shift left (or move 0 into lsb)
	button2 = button2 << 1;

	if (PIND & _BV(PD3)) button1++;						//if button pressed add 1 (or change 0 in lsb to 1)
	if (button1 == 0x00) new1 = 1;						//if 8 bits are 1, button is really pressed
	if (button1 == 0xFF) new1 = 0;

	if ((old1 == 1) && (new1 == 0))						//if button wasn't pressed and now it is
	{
		wdt_reset();
		menu_button++;									//increment menu option
	}
	old1 = new1;										//save current button state

	if (PIND & _BV(PD6)) button2++;
	if (button2 == 0x00) new2 = 1;
	if (button2 == 0xFF) new2 = 0;

	if ((old2 == 1) && (new2 == 0))
	{
		wdt_reset();
		value_button++;
	}
	old2 = new2;
}



ISR (TIMER1_OVF_vect)									//pulsewidth calculation for the LEDS
{
	static uint8_t vertraging = 0;
	static uint32_t white = 0;
	static uint32_t red = 0;
	static uint32_t  green = 0;

	if (vertraging < 1) vertraging++;					//run code every seconds time (slow down)
	else
	{
		vertraging = 0;

		if (leds_on == NOTACTIVE)
		{
			white = 0;
			red = 0;
			green = 0;
		}

		if (red > 0)										//start with the RED leds
		{
			OCR0B = red / 128;								//0 to 32767 div by 128 gives 0 tot 255
			DDRD |= (1 << PD5);								//enable RED leds port
		}
		else
		{
			OCR0B = 0;										//RED leds off
			DDRD &= ~(1 << PD5);							//disable RED leds port
			red = 0;
		}
		red++;
		if (red > 32700) red = 32700;						//max RED level = 32700/128 = 255


		if (red > 16350)									//when RED leds are at 50%
		{													//start the BLUE and GREEN leds
			OCR1B = (10000 * green) / (75535 - green);		//they are both connected together
			DDRB |= (1 << PB4);
			green++;
			if (green > 65535) green = 65535;
		}
		else
		{
			OCR1B = 0;
			DDRB &= ~(1 << PB4);
			green = 0;
		}

		if (red > 32600)									//at 100% RED led level also start WHITE leds
		{
			OCR1A = (10000 * white) / (75535 - white);
			DDRB |= (1 << PB3);
			white++;
			if (white > 65535) white = 65535;
		}
		else
		{
			OCR1A = 0;
			DDRB &= ~(1 << PB3);
			white = 0;
		}
	}
}